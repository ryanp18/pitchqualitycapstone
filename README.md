The only requirements to run our code are to have the Pybaseball, pandas, numpy, sklearn, seaborn, matplotlib, and xgboost packages installed. You will also need the data file "MLBData.csv" in the same directory as Jupyter notebook file.

The code is written sequentially so all cells can be ran in order and it will produce no errors or unintended results. However, the random grid search and cv code section will take a large amount of time to run, so it would be best to skip over that section if possible. 
